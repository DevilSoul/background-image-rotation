package com.muyun.intellij.plugin.background.image.rotation.action;

import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.muyun.intellij.plugin.background.image.rotation.BackgroundService;
import com.muyun.intellij.plugin.background.image.rotation.RandomBackgroundTask;
import com.muyun.intellij.plugin.background.image.rotation.ui.Settings;
import org.jetbrains.annotations.NotNull;

/**
 * @author Lachlan Krautz
 * @date   2016-07-21
 */
public class RandomBackground extends AnAction {
    
    public RandomBackground() {
        super("Random Background Image");
        PropertiesComponent prop = PropertiesComponent.getInstance();
        if (prop.getBoolean(Settings.AUTO_CHANGE, false)) {
            BackgroundService.start();
        }
    }
    
    @Override
    public void actionPerformed(@NotNull AnActionEvent evt) {
        PropertiesComponent prop = PropertiesComponent.getInstance();
        if (prop.getBoolean(Settings.AUTO_CHANGE, false)) {
            BackgroundService.restart();
        } else {
            new RandomBackgroundTask().run();
        }
    }
    
}
