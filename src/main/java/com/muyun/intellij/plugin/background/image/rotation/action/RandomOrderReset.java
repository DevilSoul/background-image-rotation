package com.muyun.intellij.plugin.background.image.rotation.action;

import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.muyun.intellij.plugin.background.image.rotation.BackgroundService;
import com.muyun.intellij.plugin.background.image.rotation.ImagesHandlerSingleton;
import com.muyun.intellij.plugin.background.image.rotation.RandomBackgroundTask;
import com.muyun.intellij.plugin.background.image.rotation.ui.Settings;
import org.jetbrains.annotations.NotNull;

/**
 * 重置随机图片顺序
 *
 * @author muyun12
 * @date 2020-12-24 22:58:20
 * @since 1.0.3
 */
public class RandomOrderReset extends AnAction {


    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        ImagesHandlerSingleton.INSTANCE.resetRandomImageList();
        PropertiesComponent prop = PropertiesComponent.getInstance();
        if (prop.getBoolean(Settings.AUTO_CHANGE, false)) {
            BackgroundService.restart();
        } else {
            new RandomBackgroundTask().run();
        }
    }
}
