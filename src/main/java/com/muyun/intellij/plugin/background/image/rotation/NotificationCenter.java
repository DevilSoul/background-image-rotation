package com.muyun.intellij.plugin.background.image.rotation;

import com.intellij.notification.*;

/**
 * @author Allan de Queiroz
 * @date   2017-05-07
 */
public class NotificationCenter {

    private NotificationCenter(){
        // 防止实例化
    }

    private static final NotificationGroup NOTIFICATION_GROUP = new NotificationGroup(
            "background-image-rotation", NotificationDisplayType.BALLOON, true);

    public static void notice(String message) {
        Notifications.Bus.notify(NOTIFICATION_GROUP.createNotification(
                "Background Image Rotation", message, NotificationType.INFORMATION, null));
    }

}
