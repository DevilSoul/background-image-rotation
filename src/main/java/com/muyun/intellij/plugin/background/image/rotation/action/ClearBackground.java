package com.muyun.intellij.plugin.background.image.rotation.action;

import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.wm.impl.IdeBackgroundUtil;
import com.muyun.intellij.plugin.background.image.rotation.BackgroundService;
import com.muyun.intellij.plugin.background.image.rotation.ui.Settings;
import org.jetbrains.annotations.NotNull;

/**
 * @author Lachlan Krautz
 * @date   2016-07-22
 */
public class ClearBackground extends AnAction {

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        PropertiesComponent prop = PropertiesComponent.getInstance();
        prop.setValue(IdeBackgroundUtil.EDITOR_PROP, null);
        prop.setValue(IdeBackgroundUtil.FRAME_PROP, null);
        prop.setValue(Settings.AUTO_CHANGE, false);
        BackgroundService.stop();
    }

}
