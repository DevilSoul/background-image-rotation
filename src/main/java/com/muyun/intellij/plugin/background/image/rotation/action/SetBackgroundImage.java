package com.muyun.intellij.plugin.background.image.rotation.action;

import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.DumbAwareAction;
import com.muyun.intellij.plugin.background.image.rotation.BackgroundService;
import com.muyun.intellij.plugin.background.image.rotation.ui.Settings;
import org.jetbrains.annotations.NotNull;

/**
 * 修复2020.2不兼容的问题
 *
 * @author muyun12
 * @date 2020-08-23 21:04:25
 */
public class SetBackgroundImage extends DumbAwareAction {

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        BackgroundService.stop();
        PropertiesComponent prop = PropertiesComponent.getInstance();
        if (prop.getBoolean(Settings.AUTO_CHANGE, false)) {
            BackgroundService.start();
        }
    }

}
